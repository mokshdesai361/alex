function gunController(Gun) {
    function post(req, res) {
        const gun = new Gun(req.body);
        if (!req.body.gunName) {

            gun.save();
            return res.json(gun);
        }
    }

    function get(req, res) {
        const query = {};
        if (req.query.gunName) {
            query.gunName = req.gunName;
        }
        if (req.query.gunType) {
            query.gunType = req.gunType;
        }


        Gun.find(query, (err, Gun) => {
            if (err) {
                return res.send(err);
            }
            return res.json(Gun);
        });
    }


    return {post, get};
}

module.exports = gunController;